-- This syntax is for postgresql

DROP TABLE IF EXISTS linodes_organizations;
DROP TABLE IF EXISTS users_organizations;
DROP TABLE IF EXISTS linodes;
DROP TABLE IF EXISTS organizations;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(256)
);

CREATE TABLE organizations (
    id SERIAL PRIMARY KEY,
    name VARCHAR(256)
);

CREATE TABLE linodes (
    id SERIAL PRIMARY KEY,
    type VARCHAR(128)
);

CREATE TABLE users_organizations (
    user_id INT  REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organization_id INT  REFERENCES organizations(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (user_id, organization_id)
);

CREATE TABLE linodes_organizations (
    linode_id INT  REFERENCES linodes(id) ON DELETE CASCADE ON UPDATE CASCADE,
    organization_id INT  REFERENCES organizations(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (linode_id, organization_id)
);

INSERT INTO users (email) VALUES ('dude@human.com');
INSERT INTO users (email) VALUES ('person@human.com');
INSERT INTO users (email) VALUES ('et@alien.com');
INSERT INTO users (email) VALUES ('totallyahuman@human.com');

INSERT INTO organizations (name) VALUES ('aliens');
INSERT INTO organizations (name) VALUES ('humans');

INSERT INTO users_organizations (user_id, organization_id) VALUES (1,2);
INSERT INTO users_organizations (user_id, organization_id) VALUES (2,2);
INSERT INTO users_organizations (user_id, organization_id) VALUES (3,1);
INSERT INTO users_organizations (user_id, organization_id) VALUES (4,1);
INSERT INTO users_organizations (user_id, organization_id) VALUES (4,2);

INSERT INTO linodes (type) VALUES ('standard_1GB');
INSERT INTO linodes (type) VALUES ('standard_12GB');
INSERT INTO linodes (type) VALUES ('standard_12GB');
INSERT INTO linodes (type) VALUES ('high_mem_16GB');
INSERT INTO linodes (type) VALUES ('high_mem_32GB');
INSERT INTO linodes (type) VALUES ('high_mem_32GB');

INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (1,2);
INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (2,2);
INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (3,2);
INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (4,1);
INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (5,1);
INSERT INTO linodes_organizations (linode_id, organization_id) VALUES (6,1);

-- Assumes that the sql will be called by a library
-- that sanitizes the input for email, and said library
-- uses '?' as the character for parameter replacement.

SELECT linodes.*
FROM linodes
INNER JOIN linodes_organizations lo
on lo.linode_id = linodes.id
INNER JOIN users_organizations uo
on uo.organization_id = lo.organization_id
INNER JOIN users
on users.id = uo.user_id
WHERE users.email = ?;