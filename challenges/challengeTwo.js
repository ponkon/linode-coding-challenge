import _ from 'lodash';

const deepMerge = (hash1, hash2) => {
  let result = hash2;
  _.each(hash1, (value, key) => {
    if (_.isArray(value)) {
      if(_.has(result, key)) {
        // assumes that if there's a key in hash 1 of type
        // array, and hash 2 has the key, then the value in
        // hash 2 is also of type array
        result[key] = _.concat(result[key], value)
      } else {
        result[key] = value
      }
    } else if (_.isObjectLike(hash1)) {
      if (_.isObjectLike(result[key])) {
        result[key] = deepMerge(hash1[key], result[key])      
      } else {
        result[key] = hash1[key]
      }
    } else if (!_.has(result, key)) {
      result[key] = hash1[key]
    }
  })

  return result
}

const challengeTwo = (hash1, hash2) => {
  return deepMerge(hash2, hash1)
}

export default challengeTwo