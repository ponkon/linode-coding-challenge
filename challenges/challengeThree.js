import _ from 'lodash';

const findDupes = (source, dupesFound) => {
  if (source.length < 2) {
    return dupesFound
  }

  let nextStartIndex = 1;
  for (var i = 1; i < source.length; i++) {
    if (source[0] == source[i]) {
      if (dupesFound[dupesFound.length - 1] != source[0]) {
        dupesFound.push(source[0])
      }
    } else {
      nextStartIndex = i;
      break;
    }
  }

  return findDupes(_.slice(source, nextStartIndex), dupesFound)
}

const challengeThree = (arr) => {
  let sortedArr = arr.sort((a, b) => a - b);
  let dupesFound = [];

  return findDupes(sortedArr, dupesFound)
}

export default challengeThree