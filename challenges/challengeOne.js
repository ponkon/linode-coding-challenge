import _ from 'lodash';

const reverseWord = (word) => {
  let result = '';
  for (var i = 0; i < word.length; i++) {
    result = word[i] + result;
  }
  return result;
}

const challengeOne = (sentence) => {
  let sentanceArray = _.split(sentence, ' ');
  let reversedWordSentenceArray = _.map(sentanceArray, reverseWord)

  return _.join(reversedWordSentenceArray, ' ')
}

export default challengeOne