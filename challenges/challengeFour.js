import _ from 'lodash';

const challengeFour = (str1, str2) => {
  const list1 = _.split(str1, ',');
  const list2 = _.split(str2, ',');

  const combinedList = _.map(list1, (val, index) => {
    return {l1: val, l2: list2[index]}
  })

  const sortedByList1 = _.sortBy(combinedList, 'l1');

  return _.map(sortedByList1, (item) => item.l2)
}

export default challengeFour