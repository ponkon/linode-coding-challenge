import _ from 'lodash';

const getPermutations = (left, right, permutations) => {
  if (!right.length) {
    return _.concat(permutations, _.join(left,''))
  }

  for (let i = 0; i < right.length; i++) {
    const newLeft = _.concat(left, right[i])
    const newRight = _.filter(right, (item, index) => index != i);
    permutations = getPermutations(newLeft, newRight, permutations)
  }

  return permutations
}

const challengeFive = (str) => {
  const strArr = _.toArray(str);
  return getPermutations([], strArr, [])
}

export default challengeFive