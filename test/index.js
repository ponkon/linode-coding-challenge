import { expect } from 'chai';

import challengeOne from '../challenges/challengeOne';
import challengeTwo from '../challenges/challengeTwo';
import challengeThree from '../challenges/challengeThree';
import challengeFour from '../challenges/challengeFour';
import challengeFive from '../challenges/challengeFive';
import testInputs from './Inputs.json';
import testOutputs from './Outputs.json';

describe("Linode Coding Challenges", () => {
  describe("Challenge 1", () => {
    it("Reverses each word in the sentence.", () => {
      const expectedValue = testOutputs[0];
      const actualValue = challengeOne(testInputs[0]);

      expect(actualValue).to.equal(expectedValue);
    });
  });
  describe("Challenge 2", () => {
    it("Merges the hashes.", () => {
      const expectedValue = testOutputs[1];
      const actualValue = challengeTwo(
        testInputs[1].first_hash,
        testInputs[1].second_hash
      );

      expect(actualValue).to.deep.equal(expectedValue);
    });
  });
  describe("Challenge 3", () => {
    it("Finds the duplicate values in the array.", () => {
      const expectedValue = testOutputs[2];
      const actualValue = challengeThree(testInputs[2]);

      expect(actualValue).to.deep.equal(expectedValue);
    });
  });
  describe("Challenge 4", () => {
    it("Sorts the second list by the first list.", () => {
      const expectedValue = testOutputs[3];
      const actualValue = challengeFour(testInputs[3].str1, testInputs[3].str2);

      expect(actualValue).to.deep.equal(expectedValue);
    });
  });
  describe("Challenge 5", () => {
    it("Returns all the permutations of the word \"cats\".", () => {
      const expectedValue = testOutputs[4];
      const actualValue = challengeFive(testInputs[4]);

      expect(actualValue).to.deep.equal(expectedValue);
    });
  });
});
