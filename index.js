import _ from 'lodash';
import fs from 'fs';

import challengeOne from './challenges/challengeOne';
import challengeTwo from './challenges/challengeTwo';
import challengeThree from './challenges/challengeThree';
import challengeFour from './challenges/challengeFour';
import challengeFive from './challenges/challengeFive';
import testInputs from './test/Inputs.json';

const testInputIndex = +process.env.npm_config_challenge - 1;

switch(process.env.npm_config_challenge) {
  case '1':
    console.log(challengeOne(testInputs[testInputIndex]))
    break;
  case '2':
    console.log(
      JSON.stringify(
        challengeTwo(
          testInputs[testInputIndex].first_hash,
          testInputs[testInputIndex].second_hash
        ),
        null,
        2
      )
    );
    break;
  case '3':
    console.log(
      JSON.stringify(
        challengeThree(testInputs[testInputIndex]),
        null,
        2
      )
    );
    break;
  case '4':
    console.log(JSON.stringify(
      challengeFour(
        testInputs[testInputIndex].str1,
        testInputs[testInputIndex].str2 
      ),
      null,
      2
    ));
    break;
  case '5':
    console.log(challengeFive(testInputs[testInputIndex]))
    break;
  case '6':
    console.log(fs.readFileSync(testInputs[testInputIndex]).toString('utf-8'))
    break;
  case '7':
    console.log(fs.readFileSync(testInputs[testInputIndex]).toString('utf-8'))
    break;
}
