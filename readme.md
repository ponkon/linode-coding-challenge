# Nate Sanford's Linode Coding Challenge

## Installing
First clone the repository from `https://gitlab.com/ponkon/linode-coding-challenge`

Then install the required modules with:
```bash
npm i
```

## Running a Challenge

In order to run a specific challenge with the test inputs provided in the instructions the `challenge` option needs to be passed at runtime like:

```bash
npm start --challenge=1
```

## Running Tests

To run the test suite which checks each challenge run:

```bash
npm test
```

## Changing the Challenge Inputs

If you want to change the input to the functions to something other than what was outlined in the instructions just update `test/Inputs.json` with the inputs you would like to change. Note that if inputs are changed then and the tests are run without changing the expected outputs in `test/Outputs.json` then the tests will fail.

## Assumptions

### Challenge 2

If there's an array of hashes the contents of the second array of hashes will be appended to the first, no merge will be done on the contents of arrays.